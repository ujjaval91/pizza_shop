import { actionTypes } from "../actions/actionTypes";
let user = localStorage.getItem('user')
if(user){ user = JSON.parse(user); }
  
  const initialAuthState = {
      token: localStorage.getItem('token'),
      loading: false,
      user: user,
      error: null,
      alert: null,
  };

const authReducer = (state=initialAuthState, {type, payload}) => {
      switch (type) {
        case actionTypes.REGISTER_START:
          return { ...state, loading: true};
        
        case actionTypes.REGISTER_FAIL:
          return { ...state, loading: false, error: payload};
          
        case actionTypes.REGISTER_SUCESS:
          return { ...state, loading: false, alert: { type:"success", msg:"Registration successful!!!"}};
          
        case actionTypes.LOGIN_START:
          return { ...state, loading: true};

        case actionTypes.LOGIN_SUCCESS:
          return { ...state, loading: false, token: payload.token, user: payload.user, alert: { type:"success", msg:"Login successful!!!"}};

        case actionTypes.LOGOUT:
          return { ...state, user: null, alert: { type:"success", msg:"Logout successful!!!"}};
          
        case actionTypes.LOGIN_FAILED:
        return { ...state, alert: { type:"danger", msg:"Invalid login details. Please try again."}};

        case actionTypes.CLEAR_ALERT:
        return { ...state, alert: null };

        case actionTypes.UPDATE_USER_PROFILE:
        return { ...state, user: {...payload}, alert: { type:"success", msg:"Profile has been updated successfully."}};
        
        default:
          return state;
      }
}

export default authReducer;
