import { actionTypes } from "../actions/actionTypes";

  const initialAuthState = {
      alert: { type:"", msg:""}
  };

const orderReducer = (state=initialAuthState, {type, payload}) => {
      switch (type) {
        case actionTypes.ORDER_SUCCESS:
          return { ...state, alert: { type:"success", msg:"congratulations your order has been processed!!!"}};
        
        case actionTypes.ORDER_FAIL:
          return { ...state, alert: { type:"danger", msg:payload}};
          
        case actionTypes.CLEAR_ORDER_ALERT:
          return { ...state, alert: { type:"", msg:""} };

        default:
          return state;
      }   
}

export default orderReducer;
