import axios from 'axios';
import { actionTypes } from "./actionTypes";

export const storeOrder = (data,uid) => {
    const allOrders = JSON.parse(localStorage.getItem('orders')) || [];
    allOrders.push(data);
    if(uid){
        const orders = allOrders.filter(item => item.uid !== uid)
        localStorage.setItem('orders', JSON.stringify(orders));
    }else{
        localStorage.setItem('orders', JSON.stringify(allOrders));
    }
    
}

export const getOrders = () => {
    const orders = JSON.parse(localStorage.getItem('orders')) || [];
    return orders;
}

export const removeOrder = (uid) => {
    const orders = JSON.parse(localStorage.getItem('orders'));
    const allOrders = orders.filter(item => item.uid !== uid)
    localStorage.setItem('orders', JSON.stringify(allOrders));
}

const clearCart = () => {
    localStorage.removeItem('orders');
}

export const orderCheckout = (userData) => {
    const orderArr = {};
    orderArr['user'] = userData
    orderArr['order'] = JSON.parse(localStorage.getItem('orders'));
    
    return dispatch => {
        let url = process.env.REACT_APP_API_URL+'order';
        axios.post(url,orderArr)
            .then(({data: {success} }) => {
                clearCart();
                dispatch({ type: actionTypes.ORDER_SUCCESS });
            })
            .catch(err => {
                dispatch({ type: actionTypes.ORDER_FAIL, payload: err.response.data.error ? err.response.data.error : "Some error occurred. Please try again." });
            });
    };
}

