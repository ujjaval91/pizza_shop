import axios from 'axios';
import { actionTypes } from "./actionTypes";

export const registration = (userData) => {
    return dispatch => {
        dispatch({ type: actionTypes.REGISTER_START });
        let url = process.env.REACT_APP_API_URL+'register';
        axios.post(url, userData)
            .then(({data: {success} }) => {
                dispatch({ type: actionTypes.REGISTER_SUCCESS });
            })
            .catch(err => { 
                dispatch({ type: actionTypes.REGISTER_FAIL, payload: err.response.data.error ? err.response.data.error : "Some error occurred. Please try again." });
            });
    };
}

export const login = (userData) => {
    return dispatch => {
        dispatch({ type: actionTypes.LOGIN_START });
        let url = process.env.REACT_APP_API_URL+'login';
        axios.post(url, userData)
            .then(({data: {success} }) => {
                dispatch({ type: actionTypes.LOGIN_SUCCESS, payload: success });
                if(success.token){ localStorage.setItem('token', success.token); }
                if(success.user){ localStorage.setItem('user', JSON.stringify(success.user)); }
            })
            .catch(err => {
                dispatch({ type: actionTypes.LOGIN_FAILED, payload: err.response.data.error ? err.response.data.error : "Some error occurred. Please try again." });
            });
    };
}

export const  isEmailAddressExist = async (email,id) => {
    let url = process.env.REACT_APP_API_URL+'email-verify';
    return axios.post(url,{"email":email});
}

export const logout = () => {
    localStorage.removeItem('user');
    return ({ type: actionTypes.LOGOUT });
}

export const clearAlert = () => {
    return ({ type: actionTypes.CLEAR_ALERT });
}

export const getUserDetails = () => {
    return JSON.parse(localStorage.getItem('user'));
}