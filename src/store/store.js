import { applyMiddleware, compose, createStore, combineReducers } from "redux";
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';
import authReducer from './reducers/auth';
import orderReducer from './reducers/order';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    auth: authReducer,
    order: orderReducer,
});
  
const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk, logger))
);


export default store;