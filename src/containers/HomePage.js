import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { Spinner, Alert } from 'react-bootstrap'
import PizzasCard from "../components/PizzasCard"
import PizzaModel from "../components/PizzaModel"
import { actionTypes } from "../store/actions/actionTypes";


export default function HomePage(){
  const dispatch = useDispatch();
  const [pizzasArr, setPizzasArr] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [pizzaDetail, setPizzaDetail] = useState(null);
  const [displayAlert, setdisplayAlert] = useState({status: false, variant: null, msg: false});
  const alertBox = useSelector(state => (state.order.alert) );
  
  useEffect(() => {
    if(alertBox.msg){
        setdisplayAlert({status: true, variant: alertBox.type, msg: alertBox.msg });
        const timer = setTimeout(function(){ dispatch({ type: actionTypes.CLEAR_ORDER_ALERT }); }, 1000);
        return () => clearTimeout(timer);
    }
  },[alertBox,dispatch])

  useEffect(() => {
    async function loadPizzas() {      
      let url = process.env.REACT_APP_API_URL+'getPizzas';
      const response = await axios.post(url);
      setPizzasArr(response.data.pizzas);
      setIngredients(response.data.ingredients);
    }
    loadPizzas();
  }, []);

  const onClickHandler = (detail) => {
    setPizzaDetail(detail);
  }

  const onCloseModalHandler = () => {
    setPizzaDetail(null);
  }



    let pizzasCard = null;
    if(pizzasArr.length> 0){
      pizzasCard = pizzasArr.map( item => {
        return <PizzasCard key = {item.id} {...item} clickHandler={() => onClickHandler(item)} />
      })
    }

    
    return (
        <>
          {displayAlert.status && <Alert className="my-5" variant={displayAlert.variant} onClose={() => setdisplayAlert({status: false, variant: null, msg: false})} dismissible>{displayAlert.msg}</Alert>}
          <div className="m-5 d-flex justify-content-center font-weight-bold">Add your favorite ingredients and buy now.</div>
         
          <div className="row">
            { pizzasCard ? pizzasCard : <Spinner animation="border" variant="primary" style={{ margin: "auto", marginTop: "100px", width:"100px", height:"100px"}} />} 
            
          </div>
          
          {pizzaDetail && <PizzaModel {...pizzaDetail } ingredients={ingredients} onClose={onCloseModalHandler} setdisplayAlert={setdisplayAlert}/>}
        </>
    )

}