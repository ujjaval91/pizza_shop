import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import { useSelector } from 'react-redux';
import LogoutPage from './LogoutPage'
import NotFoundPage from "./NotFoundPage";

export default function PrivatePage(){  
    const user = useSelector(state => (state.auth && state.auth.user) );
    if(!user){ return (<Redirect to="/auth/login" />); }
    return (
        <Switch>
            <Route path='/logout' component={LogoutPage}/>
            <Route component={NotFoundPage}/>
        </Switch>
    )
}