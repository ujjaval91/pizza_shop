import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import PageTitle from '../layout/PageTitle';
import { Alert, Table, Spinner } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { getOrders } from '../store/actions'
import OrderRow from '../components/OrderRow'
import { getEuroPrice } from '../utils/utils'
import { removeOrder } from '../store/actions'
import { actionTypes } from "../store/actions/actionTypes";
import PizzaModel from "../components/PizzaModel"
import PersonalDetail from "../components/PersonalDetail"

const CartPage = () => {
    const orders = getOrders();
    const dispatch = useDispatch();
    
    const [deliveryCharge, setDeliveryCharge] = useState(0);
    const [pizzasArr, setPizzasArr] = useState([]);
    const [ingredientsArr, setIngredientsArr] = useState([]);
    const [userOrder, setUserOrder] = useState(null);
    const [displayAlert, setdisplayAlert] = useState({status: false, variant: null, msg: false});
    const alertBox = useSelector(state => (state.order.alert) );
    useEffect(() => {
        if(alertBox.msg){
            setdisplayAlert({status: true, variant: alertBox.type, msg: alertBox.msg });
            const timer = setTimeout(function(){ dispatch({ type: actionTypes.CLEAR_ORDER_ALERT }); }, 1000);
            return () => clearTimeout(timer);
        }
    },[alertBox,dispatch])

    useEffect(() => {
        async function loadPizzas() {    
            const orderIds = { id: orders.map((order) => order.id) }
            let url = process.env.REACT_APP_API_URL+'getPizzas';
            const response = await axios.post(url,orderIds);
            const items = [];
            if(response.data.pizzas){
                response.data.pizzas.forEach(pizza => {
                    items[pizza.id] = pizza;
                }); 
            }
            const ingds = [];
            if(response.data.ingredients){
                response.data.ingredients.forEach(row => {
                    ingds[row.id] = row;
                }); 
            }
            setIngredientsArr(ingds);
            setPizzasArr(items);
            setDeliveryCharge(response.data.deliveryCharge);
        }
        loadPizzas();
      }, []);

    let finalPrice = 0;
    let totalQuantity = 0;
    if(pizzasArr.length > 0){
        orders.forEach(order => {
            totalQuantity = parseInt(totalQuantity) + parseInt(order.quantity)
            finalPrice = parseFloat(finalPrice) + parseFloat(pizzasArr[order.id].price) * order.quantity;
            if(order.ingredients.length > 0){
                order.ingredients.forEach(iId => {
                    finalPrice = parseFloat(finalPrice) + parseFloat(ingredientsArr[iId].price) * order.quantity;
                })
            }
        })
        finalPrice = parseFloat(finalPrice) + parseFloat(deliveryCharge);
        
    }
    const onCloseModalHandler = () => {
        setUserOrder(null);
    }

    const editItemHandler = (uid,id) => {
        const pizz = orders.find((odr) => (odr.uid === uid && odr.id===id));
        setUserOrder(pizz);
    }
    const onRemoveIteam = (uid) => {
        removeOrder(uid)
        setUserOrder(null);
        setdisplayAlert({status: true, variant:"primary", msg: "Your item has been successfully removed form your cart."})
    }
    
    return (
        <>
            <PageTitle title="Order Summary"/>            
            <div className="row">
            {displayAlert.status && <Alert style={{width: "100%"}} variant={displayAlert.variant} onClose={() => setdisplayAlert({status: false, variant: null, msg: false})} dismissible>{displayAlert.msg}</Alert>}
            {(pizzasArr.length > 0 && orders.length > 0) ? (
                <div className="col-sm-12">
                    <Table bordered hover responsive className="mt-3">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Items</th>
                            <th>Price per item</th>
                            <th>Quantity</th>
                            <th>Final Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        {orders.map((order,key) => <OrderRow key={key} sno={key} {...order} ingredientsArr={ingredientsArr} pizzaDetail={pizzasArr[order.id]} editItemHandler={editItemHandler} />)}
                        <tr>
                            <td></td>
                            <td colSpan="3">Delivery Charges</td>
                            <td>${deliveryCharge} <small>(&euro;{getEuroPrice(deliveryCharge)})</small></td>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th colSpan="2">Total</th>
                            <th>{parseInt(totalQuantity)}</th>
                            <th>${parseFloat(finalPrice).toFixed(2)} <small>(&euro;{getEuroPrice(finalPrice)})</small></th>
                        </tr>
                        </tbody>
                    </Table>
                    <PersonalDetail />
                </div>
            ) : (orders.length < 1) ? <Alert style={{width: "100%"}} variant='primary'>Your cart is empty. Please add atlest one item in your cart. Visit <Link to='/'>Home</Link></Alert> : <Spinner animation="border" variant="primary" style={{ margin: "auto", marginTop: "100px", width:"100px", height:"100px"}} /> }
            </div>
            {userOrder && <PizzaModel {...pizzasArr[userOrder.id] } userOrder={userOrder} ingredients={ingredientsArr} onClose={onCloseModalHandler} onRemove={onRemoveIteam} setdisplayAlert={setdisplayAlert}/>}
        </>
    )
}
export default CartPage;