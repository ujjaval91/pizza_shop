import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect  } from "react-router-dom";
import { logout } from "../store/actions";

export default function Logout(){
    const userDetail = useSelector(state => state.auth && state.auth.user );
    const dispatch = useDispatch();

    useEffect(() => { 
        dispatch(logout()); 
    }, [dispatch]);
    
    return userDetail ? "logout" : <Redirect to="/login" />;
}