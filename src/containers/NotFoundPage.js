import React from 'react';
import PageTitle from '../layout/PageTitle';

const NotFoundPage = () => (<><PageTitle title="Page Not Found"/><p>Page not found</p></>)

export default NotFoundPage;