import React, { useState, useEffect } from 'react';
import { Link, Redirect  } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { createBrowserHistory } from 'history';
import { Form, Alert } from 'react-bootstrap';
import PageTitle from '../layout/PageTitle';
import Input from '../components/Input'
import { login } from "../store/actions";
import { actionTypes } from "../store/actions/actionTypes";
import validateForm from '../utils/validateForm';

export default function LoginPage(){
    const browserHistory = createBrowserHistory();
    const [displayAlert, setdisplayAlert] = useState(false);
    const dispatch = useDispatch();
    const alertbox = useSelector(state => state.auth && state.auth.alert );    
    const user = useSelector(state => (state.auth && state.auth.user) );
    
    useEffect(() => {
        browserHistory.listen((location, action) => {
            dispatch({ type: actionTypes.CLEAR_ALERT });
        });
    },[browserHistory,dispatch]);
    
    const clearAlert = () => {
        dispatch({ type: actionTypes.CLEAR_ALERT });
    }

    let defaultField = {
        elementType: 'input',
        elementConfig: { type: 'text' },
        value: '',
        valid: false,
        errorMessage: null,
        touched: false
    }

    let formConfig = {
        formFields: { 
            email: {
                ...defaultField,
                title: 'Email',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Email',
                    maxLength: '50'
                },
                validation: {
                    required: true,
                    pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Invalid Email"
                    },
                }
            },   
            password: {
                ...defaultField,
                title: 'Password',
                elementType: 'input',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Password',
                    type: 'password',
                    maxLength: '30'
                },
                validation: {
                    required: true,
                },
            },
        },
        formIsValid: false,
        sucessMessage: false,
        errorMessage: false,
    }
    
    const [formState, setformState] = useState(formConfig);

    if(user){
        return (<Redirect to="/dashboard" />);
    }


    let formConfigArr = Object.values(formState.formFields);
    let formConfigKeyArr = Object.keys(formState.formFields);
    
    const inputChangeHandler = (event,fieldKey)  => {
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.value = event.target.value;
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }


    const inputBlurHandler = (event,fieldKey)  => {
        setdisplayAlert(false);
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.touched = true;
        const validate = validateForm(elementValues.value,elementValues.validation);
        elementValues.valid = validate.isValid
        elementValues.errorMessage = validate.message
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }

    const formSubmitHandler = (event) => {
        event.preventDefault();
        const formElements = { ...formState };

        let formIsValid = true;
        for (let inputIdentifier in formElements['formFields']) {
            if(formElements['formFields'][inputIdentifier].validation){
                const validate = validateForm(formElements['formFields'][inputIdentifier].value,formElements['formFields'][inputIdentifier].validation);                
                formElements['formFields'][inputIdentifier].touched = true
                formElements['formFields'][inputIdentifier].valid = validate.isValid
                formElements['formFields'][inputIdentifier].errorMessage = validate.message
                if(formIsValid && !validate.isValid){
                    formIsValid = validate.isValid
                }
            }
        }

        if(formIsValid){
            const userData = {};
            formConfigKeyArr.forEach(fldName => {
                userData[fldName] = formElements.formFields[fldName].value;
            });
            dispatch(login(userData));
        }else{
            formElements.formIsValid = formIsValid;
            formElements.errorMessage = "Please fill all the required fields and try again.";
            setformState(formElements);
            setdisplayAlert(true);
        }
    }

    let formContaint = (
        <div className="mt-md-4 mt-sm-1">
        <Form className="form-horizontal" role="form" onSubmit={formSubmitHandler}>
            { formConfigArr.map((formField,key) => (
                <div key={"a_"+key}>
                    <Input 
                        key={key}
                        name={formConfigKeyArr[key]}
                        title={formField.title}
                        elementType={formField.elementType}
                        elementConfig={formField.elementConfig}
                        value={formField.value}
                        invalid={!formField.valid}
                        shouldValidate={formField.validation}
                        errorMessage={formField.errorMessage}
                        touched={formField.touched}
                        changed={(event) => inputChangeHandler(event,formConfigKeyArr[key])}
                        onblur={(event) => inputBlurHandler(event,formConfigKeyArr[key])}
                        />
                </div>
                ))
            }
            <div className="row mt-5 mb-5">                        
                <div className="col-sm-12">
                    <button type="submit" className="btn btn-primary">Login</button>
                    &nbsp;&nbsp;
                    <Link to='/auth/registration' className="btn btn btn-link">Registration</Link>
                </div>
            </div>
        </Form> 
        </div>
    );

    let alertDisplay = null;
    if(alertbox && alertbox.type && alertbox.msg){
        alertDisplay = <Alert variant={alertbox.type} onClose={() => clearAlert(false)} dismissible>{alertbox.msg}</Alert>
    }else{
        if((displayAlert && (formState.errorMessage || formState.sucessMessage ))){
            alertDisplay = <Alert variant={(formState.errorMessage) ? "danger": "success"} onClose={() => setdisplayAlert(false)} dismissible>{(formState.errorMessage) ? formState.errorMessage: formState.sucessMessage}</Alert>
        }
    }
    

    return (
        <div className="col-lg-6 offset-lg-3">
            <PageTitle title="Login"/>
            {alertDisplay}
            {formContaint} 
        </div>
    )
}