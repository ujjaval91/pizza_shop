import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import LoginPage from './LoginPage'
import RegistrationPage from './RegistrationPage'

export default function AuthPage(){
    return (
        <Switch>
            <Route exact path='/auth' component={LoginPage}/>
            <Route path='/auth/login' component={LoginPage}/>
            <Route path='/auth/registration' component={RegistrationPage}/>
            <Redirect to="/auth/login" />
        </Switch>
    )
}