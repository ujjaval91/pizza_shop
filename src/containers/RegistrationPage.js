import React, { useState } from 'react';
import { Link, Redirect, useHistory  } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { Form, Alert } from 'react-bootstrap';
import PageTitle from '../layout/PageTitle';
import Input from '../components/Input'
import { registration, isEmailAddressExist } from "../store/actions";
import validateForm from '../utils/validateForm';

function RegistrationPage(){
    const dispatch = useDispatch();
    const history = useHistory();
    let defaultField = {
        elementType: 'input',
        elementConfig: { type: 'text' },
        value: '',
        valid: false,
        errorMessage: null,
        touched: false
    }

    let formConfig = {
        formFields: {
            name: {
                ...defaultField,
                title: 'Name',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Name',
                    maxLength: '50'
                },
                validation: {
                    required: true,
                }
            },   
            email: {
                ...defaultField,
                title: 'Email',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Email',
                    maxLength: '50'
                },
                validation: {
                    required: true,
                    pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Invalid Email"
                    },
                }
            },   
            password: {
                ...defaultField,
                title: 'Password',
                elementType: 'input',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Password',
                    type: 'password',
                    maxLength: '20'
                },
                validation: {
                    required: true,
                    minLength: {
                        length: 6,
                        message: 'Passwords must be at least 6 characters long.',
                    },
                },
            },
            gender: {
                ...defaultField,
                title: 'Gender',
                elementType: 'radio',
                value:'M',
                elementConfig: {
                    options: [
                        {value: 'M', displayValue: 'Male'},
                        {value: 'F', displayValue: 'Female'},
                        {value: 'O', displayValue: 'Other'}
                    ]
                },
                valid: true,
            }
        },
        formIsValid: false,
        sucessMessage: false,
        errorMessage: false,
    }
    
    const [displayAlert, setdisplayAlert] = useState(false);
    const [formState, setformState] = useState(formConfig);

    const user = useSelector(state => (state.auth && state.auth.user) );
    if(user){
        return (<Redirect to="/dashboard" />);
    }

    let formConfigArr = Object.values(formState.formFields);
    let formConfigKeyArr = Object.keys(formState.formFields);
    
    const inputChangeHandler = (event,fieldKey)  => {
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.value = event.target.value;
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }

    const inputBlurHandler = (event,fieldKey)  => {
        setdisplayAlert(false);
        const fieldValue = event.target.value;
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.touched = true;
        const validate = validateForm(elementValues.value,elementValues.validation);
        elementValues.valid = validate.isValid
        elementValues.errorMessage = validate.message
        if(fieldKey === 'email' && validate.isValid ){
            isEmailAddressExist(fieldValue)
                .then(({data: {success} }) => {
                        if(success.status){
                            const formElements = {...formState };
                            const elementValues = {...formState['formFields']['email']};
                            elementValues.valid = false
                            elementValues.errorMessage = 'Email address already exist';
                            formElements['formFields'][fieldKey] = elementValues;
                            setformState(formElements);
                        }
                    }
                );
        }
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }

    const formSubmitHandler = (event) => {
        event.preventDefault();
        const formElements = { ...formState };
        let formIsValid = true;
        for (let inputIdentifier in formElements['formFields']) {
            if(formElements['formFields'][inputIdentifier].validation){
                const validate = validateForm(formElements['formFields'][inputIdentifier].value,formElements['formFields'][inputIdentifier].validation);                
                formElements['formFields'][inputIdentifier].touched = true
                formElements['formFields'][inputIdentifier].valid = validate.isValid
                formElements['formFields'][inputIdentifier].errorMessage = validate.message
                if(formIsValid && !validate.isValid){
                    formIsValid = validate.isValid
                }
            }
        }
        if(formIsValid){
            const userData = {};
            formConfigKeyArr.forEach(fldName => {
                userData[fldName] = formElements.formFields[fldName].value;
            });
            dispatch(registration(userData));
            const initialState = { ...formConfig };
            setformState(initialState);
            setdisplayAlert(true);
            history.push('/auth/login');
        }else{
            formElements.formIsValid = formIsValid;
            formElements.errorMessage = "Please fill all the required fields and try again.";
            setformState(formElements);
            setdisplayAlert(true);
        }
    }

    

    let formContaint = (
        <div className="mt-md-4 mt-sm-1">
        <Form className="form-horizontal" role="form" onSubmit={formSubmitHandler}>
            { formConfigArr.map((formField,key) => (
                <div key={"a_"+key}>
                    <Input 
                        name={formConfigKeyArr[key]}
                        title={formField.title}
                        elementType={formField.elementType}
                        elementConfig={formField.elementConfig}
                        value={formField.value}
                        invalid={!formField.valid}
                        shouldValidate={formField.validation}
                        errorMessage={formField.errorMessage}
                        touched={formField.touched}
                        changed={(event) => inputChangeHandler(event,formConfigKeyArr[key])}
                        onblur={(event) => inputBlurHandler(event,formConfigKeyArr[key])}
                        />
                </div>
                ))
            }
            <div className="row mb-5">                        
                <div className="col-sm-12">
                    <button type="submit" className="btn btn-primary">Registration</button>
                    &nbsp;&nbsp;
                    <Link to='login' className="btn btn btn-link">Login</Link>
                </div>
            </div>
        </Form>
        </div>
    );

    let alertDisplay = null;
    if((displayAlert && (formState.errorMessage || formState.sucessMessage ))){
        alertDisplay = <Alert variant={(formState.errorMessage) ? "danger": "success"} onClose={() => setdisplayAlert(false)} dismissible>{(formState.errorMessage) ? formState.errorMessage: formState.sucessMessage}</Alert>
    }
    return (
        <div className="col-lg-6 offset-lg-3">
            <PageTitle title="Registration"/>
            {alertDisplay}
            {formContaint} 
        </div>
    )
}

export default RegistrationPage;