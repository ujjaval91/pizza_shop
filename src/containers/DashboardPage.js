import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import PageTitle from '../layout/PageTitle';
import { actionTypes } from "../store/actions/actionTypes";
import { Alert, Table, Spinner } from 'react-bootstrap';
import { getEuroPrice } from '../utils/utils'


export default function DashboardPage(){    
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(true);
    const [orders, setOrders] = useState([]);
    const [displayAlert, setdisplayAlert] = useState({status: false, variant: null, msg: false});
    const userDetail = useSelector(state => state.auth && state.auth );    
    const alertBox = useSelector(state => (state.order.alert) );

    useEffect(() => {
        if(alertBox.msg){
            setdisplayAlert({status: true, variant: alertBox.type, msg: alertBox.msg });
            const timer = setTimeout(function(){ dispatch({ type: actionTypes.CLEAR_ORDER_ALERT }); }, 1000);
            return () => clearTimeout(timer);
        }
    },[alertBox,dispatch])
    
    const userId = userDetail.user.id;   
    useEffect(() => {
        async function loadOrders() {    
            let url = process.env.REACT_APP_API_URL+'orders';
            const response = await axios.post(url,{id:userId},{'headers': { 'Authorization': 'Bearer '+userDetail.token }});
            setOrders(response.data.orders);
            setLoader(false);
        }
        loadOrders();
    }, [userId, userDetail.token]);

    let tableHtml = <tr><td colSpan="6" className="text-center">No orders found</td></tr>;
    if(orders && Object.values(orders).length > 0){
        tableHtml = Object.values(orders).map((order,key) => {
            return (
                <tr key={key}>
                    <td>{key+1}</td>
                    <td>{order.order_no}</td>
                    <td>{order.items.map(item => item.title).join(", ")}</td>
                    <td>${parseFloat(order.price).toFixed(2)} <small>(&euro;{getEuroPrice(order.price)})</small></td>
                    <td>{order.contact_number ? order.contact_number : "N/A"}</td>
                    <td>{order.address ? order.address : "N/A"}</td>
                </tr>
                )
        });
    }

    return (
        <>
            <PageTitle title="Dashboard"/>          
            <div className="row">
                {displayAlert.status && <Alert style={{width: "100%"}} variant={displayAlert.variant} onClose={() => setdisplayAlert({status: false, variant: null, msg: false})} dismissible>{displayAlert.msg}</Alert>}
                <div className="col-sm-12"><h3 className="mt-2 mb-5">My History</h3></div>
                {loader ? <Spinner animation="border" variant="primary" style={{ margin: "auto", marginTop: "100px", width:"100px", height:"100px"}} /> : (
                    <div className="col-sm-12">
                    <Table bordered hover responsive>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order No</th>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Name</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableHtml}
                        </tbody>
                    </Table></div>)}
            </div>
        </>
    )
}