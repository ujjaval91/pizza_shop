import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { useSelector } from 'react-redux';
import Layout from './layout/Layout';
import Home from './containers/HomePage'
import DashboardPage     from './containers/DashboardPage'
import CartPage from './containers/CartPage'
import PrivatePage from './containers/PrivatePage'
import AuthPage from './containers/AuthPage'

import "bootstrap/dist/css/bootstrap.css"; 

export default function App() {
  const user = useSelector(state => (state.auth && state.auth.user) );
  return (
    <BrowserRouter >
      <Layout >
          <Switch>
              <Route exact  path='/' component={Home}/>
              <Route path='/cart' component={CartPage}/>
              <Route path='/dashboard' component={DashboardPage}/>
              <Route path='/auth' component={AuthPage}/>
              {user ? (<PrivatePage />) : ( <Redirect to="/auth/login" /> )}
          </Switch>
      </Layout>
    </BrowserRouter>
  );
}
