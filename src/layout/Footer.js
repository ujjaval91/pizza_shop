import React from 'react';

export default function Footer(){
  const today = new Date();
    return(
        <footer className="container-fluid text-center bg-dark" style={{color: "white"}}>
            <div className="footer-copyright text-center py-3">&copy; {today.getFullYear()} Copyright</div>
        </footer>
    )
}