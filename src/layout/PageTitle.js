import React from 'react';

export default function PageTitle({title}){
    return(
        <div className="row">
            <div className="col-sm-12" style={{'margin': '20px 0'}}>
                <h1>{title}</h1>
            </div>
        </div>
    )
}