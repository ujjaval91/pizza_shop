import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from "react-router-dom";
import {Navbar, Nav, Container } from 'react-bootstrap'
    
const Header = () => {
    const userDetail = useSelector(state => state.auth && state.auth.user );
    return(
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
              <Container> 
                <Navbar.Brand href="/">Pizza Shop</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  <Nav className="mr-auto"></Nav>
                  <Nav>
                  <NavLink exact className="nav-link" to="/" >Home</NavLink>
                  { userDetail && <NavLink className="nav-link" to="/dashboard" >Dashboard</NavLink>}
                    <NavLink className="nav-link" to="/cart" >My Cart</NavLink>
                    { userDetail ? <NavLink className="nav-link" to="/logout">Logout</NavLink>: <NavLink className="nav-link" to="/auth/login" >Login</NavLink>}
                    { !userDetail && <NavLink className="nav-link" to="/auth/registration">Registration  </NavLink>}
                  </Nav>
                </Navbar.Collapse>
              </Container>
          </Navbar>
    )
}

export default Header;