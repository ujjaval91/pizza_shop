import React from 'react';
import Header from './Header';
import Footer from './Footer';

export default function Layout({children}){
    const layoutStyle = { 
        'minHeight': window.innerHeight - 112
    }
    return(
        <>
            <Header />
            <div className="container" style={layoutStyle}>
                {children}
            </div>
            <Footer />
        </>
    )
}