
const validateForm = (value,rules) => {
    const result = { isValid: true,message: ''};

    if (rules.required && value.trim() === ''){
        result.isValid = false;
        result.message = "This field is required";
    }

    if (result.isValid && rules.minLength && value.length < rules.minLength.length){
        result.isValid = false;
        result.message = rules.minLength.message || `Should be ${rules.minLength.length} characters long.`;
    }

    if (result.isValid && rules.maxLength && value.length > rules.maxLength.length){
        result.isValid = false;
        result.message = rules.maxLength.message || `Should not be ${rules.maxLength.length} characters long.`;
    }

    if (result.isValid && rules.pattern && !rules.pattern.value.test(value)) {
        result.isValid = false;
        result.message = rules.pattern.message || "Invalid value";
    }
    return result;
}

export default validateForm;