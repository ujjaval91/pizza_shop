
export function getEuroPrice(price) {
    return  parseFloat(price * 0.93).toFixed(2);
  }