import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-c4fe6.firebaseio.com/'
});

export default instance;