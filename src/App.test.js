import React from 'react';
import ReactDOM from "react-dom";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from './App';
import * as MyContextModule from './context/UserContext'

Enzyme.configure({ adapter: new Adapter() }); 

const MyComponent = () => {
    const myContext = MyContextModule.useMyContext();
    return <div>{parseFloat(myContext.user.accountBalance).toFixed(2)}</div>;
};

test("Renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


  
test("Renders user Account Balance", () => {
  jest.spyOn(MyContextModule, "useMyContext").mockImplementation(() => (MyContextModule.initialState));
  const wrapper = shallow(
	<MyContextModule.UserContext.Provider>
	  <MyComponent />
	</MyContextModule.UserContext.Provider>
  ).dive();
  expect(wrapper.text()).toEqual("0.00");
});