import React from 'react'
import { Button, Card } from 'react-bootstrap'

const PizzasCard = (props) => {
return (
    <div className="col-md-4 col-xs-6 mb-4">
        <Card>
            <Card.Img  className="img-fluid" variant="top" src={props.image} />
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                <Card.Text style={{minHeight: '80px'}}>{props.description}</Card.Text>
                <Button variant="primary" onClick={props.clickHandler}>Select</Button>
            </Card.Body>
        </Card>
    </div>
    );
}

export default PizzasCard;