import React, { useState } from 'react';
import { Link, useHistory  } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { Form, Alert } from 'react-bootstrap';
import Input from '../components/Input'
import { orderCheckout, getUserDetails } from "../store/actions";
import validateForm from '../utils/validateForm';

const PersonalDetail = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const userDetails = getUserDetails();
    let defaultField = {
        elementType: 'input',
        elementConfig: { type: 'text' },
        value: '',
        valid: false,
        errorMessage: null,
        touched: false
    }

    let formConfig = {
        formFields: {
            name: {
                ...defaultField,
                title: 'Name',
                value: (userDetails && userDetails.name) ? userDetails.name : '',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Name',
                    maxLength: '50'
                },
                validation: {
                    required: true,
                }
            },   
            email: {
                ...defaultField,
                title: 'Email',
                value: (userDetails && userDetails.email) ? userDetails.email : '',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Email',
                    maxLength: '50'
                },
                validation: {
                    required: true,
                    pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Invalid Email"
                    },
                }
            },     
            number: {
                ...defaultField,
                title: 'Contact Number',
                elementType: 'input',
                elementConfig: {
                    ...defaultField.elementConfig,
                    type: 'tel',
                    placeholder: 'Enter Contact Number',
                    maxLength: '10'
                },
                validation: {
                    required: true,
                    minLength: {
                        length: 10,
                        message: 'Phone number must be 10 digit long',
                    },
                    pattern: {
                        value: /^[0][1-9]\d{9}$|^[1-9]\d{9}$/,
                        message: "Invalid Contact Number"
                    },
                }
            },
            address: {
                ...defaultField,
                title: 'Address',
                elementConfig: {
                    ...defaultField.elementConfig,
                    placeholder: 'Enter Address',
                    maxLength: '250'
                },
                validation: {
                    required: true,
                }
            },   
        },
        formIsValid: false,
    }
    
    const [formState, setformState] = useState(formConfig);
    const [loading, setLoading] = useState(false);
    const [displayAlert, setdisplayAlert] = useState({status: false, variant: null, msg: false});

    const alertBox = useSelector(state => (state.order.alert));
    if(loading && alertBox.msg){ setLoading(false) }
    
    let formConfigArr = Object.values(formState.formFields);
    let formConfigKeyArr = Object.keys(formState.formFields);
    
    const inputChangeHandler = (event,fieldKey)  => {
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.value = event.target.value;
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }

    const inputBlurHandler = (event,fieldKey)  => {
        setdisplayAlert(false);
        const formElements = {...formState };
        const elementValues = {...formState['formFields'][fieldKey]};
        elementValues.touched = true;
        const validate = validateForm(elementValues.value,elementValues.validation);
        elementValues.valid = validate.isValid
        elementValues.errorMessage = validate.message
        formElements['formFields'][fieldKey] = elementValues;
        setformState(formElements);
    }

    const formSubmitHandler = (event) => {
        event.preventDefault();
        const formElements = { ...formState };
        let formIsValid = true;
        for (let inputIdentifier in formElements['formFields']) {
            if(formElements['formFields'][inputIdentifier].validation){
                const validate = validateForm(formElements['formFields'][inputIdentifier].value,formElements['formFields'][inputIdentifier].validation);                
                formElements['formFields'][inputIdentifier].touched = true
                formElements['formFields'][inputIdentifier].valid = validate.isValid
                formElements['formFields'][inputIdentifier].errorMessage = validate.message
                if(formIsValid && !validate.isValid){
                    formIsValid = validate.isValid
                }
            }
        }
        if(formIsValid){
            setLoading(true)
            const userData = {};
            userData['id'] = (userDetails && userDetails.id) ? userDetails.id : '';
            formConfigKeyArr.forEach(fldName => {
                userData[fldName] = formElements.formFields[fldName].value;
            });
            dispatch(orderCheckout(userData));
            history.push('/');
        }else{
            formElements.formIsValid = formIsValid;
            setformState(formElements);
            setdisplayAlert({status: true, variant:'danger', msg:"Please fill all the required fields and try again."});
        }
    }

    return (
        <>
            <h3 className="mt-2 mb-3">Delivery Details</h3>
            {displayAlert.status && <Alert variant={displayAlert.variant} onClose={() => setdisplayAlert({status: false, variant: null, msg: false})} dismissible>{displayAlert.msg}</Alert>}
            <Form className="form-horizontal" role="form" onSubmit={formSubmitHandler}>
                { formConfigArr.map((formField,key) => (
                    <div key={key}>
                        <Input 
                            name={formConfigKeyArr[key]}
                            title={formField.title}
                            elementType={formField.elementType}
                            elementConfig={formField.elementConfig}
                            value={formField.value}
                            invalid={!formField.valid}
                            shouldValidate={formField.validation}
                            errorMessage={formField.errorMessage}
                            touched={formField.touched}
                            changed={(event) => inputChangeHandler(event,formConfigKeyArr[key])}
                            onblur={(event) => inputBlurHandler(event,formConfigKeyArr[key])}
                            />
                    </div>
                    ))
                }
                <div className="row mb-5">                        
                    <div className="col-sm-12">
                    <button type="submit" className="btn btn-primary">{loading ? (<><span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...</>) : "Process" }</button>
                        &nbsp;&nbsp;
                        <Link to='/' className="btn btn btn-link">Back</Link>
                    </div>
                </div>
            </Form>
        </>
    )
}

export default PersonalDetail;