import React from 'react';
import { Form,Col,Row } from 'react-bootstrap';

export default function Input(props){
    let inputElement = null;

    let errorAttr = false;
    if(props.touched && props.invalid){ errorAttr = true; }

    switch (props.elementType) {
        case ('input'):
            inputElement = <Form.Control sm="8" isInvalid={errorAttr} type={props.type} {...props.elementConfig} value={props.value} onChange={props.changed} onBlur={props.onblur}/>;
            break;
        case ('radio'):
            inputElement = <>{ props.elementConfig.options.map((option,id) => (
                                <Form.Check inline
                                    key={props.name+"_"+option.value}
                                    type={props.elementType}
                                    label={option.displayValue}
                                    name={props.name}
                                    checked={(props.value === option.value)}
                                    id={props.name+"_"+option.value}
                                    value={option.value}
                                    onChange={props.changed}
                                    />
                                ))
                            }</>
            break;
            
        default:
            break;

    }
    
    return(
        <>
        <Form.Group as={Row} className={(props.touched && props.invalid ? 'has-error': '')}>
            <Form.Label column sm="2">{props.title}:</Form.Label>            
            <Col sm="10">
                { inputElement }
                {(props.touched && props.invalid && props.errorMessage && <div className="help-block text-danger">{props.errorMessage}</div>)}
            </Col>
        </Form.Group>
        </>
    );
};