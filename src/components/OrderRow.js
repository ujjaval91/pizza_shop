import React from 'react'
import parse from 'html-react-parser';

import { getEuroPrice } from '../utils/utils'

const OrderRow = (props) => {
    let itemPrice = parseFloat(props.pizzaDetail.price)
    let itemTitle = ''
    const toppingsArr = []
    if(props.ingredients.length > 0){
        props.ingredients.forEach(iId => {
            itemPrice = itemPrice + parseFloat(props.ingredientsArr[iId].price);
            toppingsArr.push(props.ingredientsArr[iId].title)
        });
        
        itemTitle = itemTitle+"<br/><small><b>Extra Toppings:</b> "+toppingsArr.join(", ")+"</small>";
    }   
    return (
        <tr>
            <td>{props.sno + 1}</td>
            <td><b><span style={{color:'#007bff', 'textDecoration': 'none', cursor:'pointer'}} onClick={()=> props.editItemHandler(props.uid,props.id)}>{props.pizzaDetail.title}</span></b>{parse(itemTitle)}</td>
            <td>${parseFloat(itemPrice).toFixed(2)} <small>(&euro;{getEuroPrice(itemPrice)})</small></td>
            <td>{props.quantity}</td>
            <td>${(itemPrice * props.quantity).toFixed(2)} <small>(&euro;{getEuroPrice(itemPrice * props.quantity)})</small></td>
        </tr>
    )    
}

export default OrderRow;