import React, { useState} from 'react';
import { Button, Modal } from 'react-bootstrap'
import { getEuroPrice } from '../utils/utils'
import { storeOrder } from '../store/actions'

const PizzaModel = (props) => {
    const uid = (props.userOrder && props.userOrder.uid) ? props.userOrder.uid : null;
    const ingredientDetails = [];
    const checkedIngred = {};
    if(props.ingredients.length > 0){
        props.ingredients.forEach((ingredient,key) =>{
            ingredientDetails[ingredient.id] = ingredient
            checkedIngred[ingredient.id] = false
        })
        /* For update */
        if(props.userOrder && props.userOrder.ingredients){
            props.userOrder.ingredients.forEach((i) => {
                checkedIngred[i] = true
            })
        }
    }
    const defaultQty = (props.userOrder && props.userOrder.quantity) ? props.userOrder.quantity : 1
    const [ quantity, setQuentity] = useState(defaultQty);
    const [ checkedIngreds, setCheckedIngreds] = useState(checkedIngred);

    const handleChange = (e,id) => {
        const tmpCheckedIngreds = {...checkedIngreds}
        tmpCheckedIngreds[id] = e.target.checked;
        setCheckedIngreds(tmpCheckedIngreds)
    }

    let extraIngredients = null;
    if(props.ingredients.length> 0){
        extraIngredients = <div className="row m-2">
                                <p><b>Would you like to add extra ingredients?</b></p>
                                {props.ingredients.map((ingredient,key) => {
                                    return (<div className="col-md-6" key={key}>
                                                <input type="checkbox"  id={`ingdnt_${ingredient.id}`} className="form-check-input" checked={checkedIngreds[ingredient.id]} onChange={(e) =>handleChange(e,ingredient.id)}/>
                                                <label htmlFor={`ingdnt_${ingredient.id}`} className="form-check-label">{ingredient.title} <small>(${ parseFloat(ingredient.price).toFixed(2)})</small></label>
                                            </div>)
                                })}
                            </div>
    }

    const calculateFinalPrice = () => {
        let ingredientsPrice = 0;
        if(checkedIngreds){
            Object.keys(checkedIngreds).forEach((checkedId) => {
                if(checkedIngreds[checkedId]){
                    ingredientsPrice = parseFloat(ingredientDetails[checkedId].price) + parseFloat(ingredientsPrice)    
                }
            })
        } 
        const finalPrice = (parseFloat(props.price) + ingredientsPrice) * quantity;
        return parseFloat(finalPrice).toFixed(2);
    }
    const finalPrice = calculateFinalPrice();

    const addToCart = (uid) => {
        const selectedIngredients = [];
        for (const value in checkedIngreds) {
            if(checkedIngreds[value]){ selectedIngredients.push(value); }
        }
        const items = {
            uid: new Date().getTime(),
            id: props.id,
            quantity: quantity,
            ingredients: selectedIngredients
        }
        storeOrder(items,uid)
        props.onClose();
        const alertSucessMsg  = uid ? "Congratulations your item has been successfully updated." : ("Congratulations your item has been successfully added. Visit your cart.")
        props.setdisplayAlert({status: true, variant:'success', msg: alertSucessMsg});
    }


    return (
        <Modal show={true} onHide={props.onClose}>
            <Modal.Body>
            <div>
                <img src={props.image} style={{width:'100%'}} alt={props.title} />
                <div style={{position:"absolute", padding:'10px 20px', top:'28px', background: 'rgba(255,255,255,0.8)'}}>{props.title}</div>
                {extraIngredients}
                <div className="row">
                    <div className="col-12 qty-counter">                
                        Quantity: 
                        <input type="button" value="-" onClick={() => setQuentity(quantity - 1)} disabled={quantity === 1} style={{margin:'0px 10px', width:'30px'}}/>
                        {quantity} 
                        <input type="button" value="+" onClick={() => setQuentity(quantity + 1)} style={{margin:'0px 10px', width:'30px'}}/>
                    </div>
                    <div className="col-12 mt-2">
                        <span className="">Price: ${parseFloat(finalPrice)} <small>(&euro;{getEuroPrice(finalPrice)})</small></span>
                    </div>
                </div>
                
            </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.onClose}>Close</Button>
                {uid && <Button variant="danger" onClick={() => props.onRemove(uid)}>Remove Item</Button>}
                <Button variant="primary" onClick={() => addToCart(uid)}>{ uid ? "Update cart" : "Add to cart"}</Button>
            </Modal.Footer>
        </Modal>          
    );
}

export default PizzaModel
          